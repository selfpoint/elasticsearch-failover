'use strict';

const HttpConnector = require('elasticsearch/src/lib/connectors/http'),
    util = require('util'),
    qs = require('qs');

module.exports = FailoverHttpConnector;

function FailoverHttpConnector(host, config) {
    if (!config.failover) {
        throw new Error('FailoverHttpConnector must have failover property in the config');
    }

    this._failover = {
        client: config.failover.client,
        retryTime: config.failover.retryTime || 600000 // default to 10 minutes
    };
    HttpConnector.call(this, host, config);
}
util.inherits(FailoverHttpConnector, HttpConnector);

FailoverHttpConnector.prototype.request = request;

FailoverHttpConnector.prototype.makeReqParams = makeReqParams;

function request(params, cb) {
    let self = this,
        abortRequest = HttpConnector.prototype.request.call(this, params, cb);

    if (self._lastAbortRequest && new Date() - self._lastAbortRequest >= this._failover.retryTime) {
        HttpConnector.prototype.request.call(this, util._extend({failover: false}, params), function (err) {
            if (!err) delete self._lastAbortRequest;
        });
    }

    return () => {
        self._lastAbortRequest = new Date();
        abortRequest();
    };
}

function makeReqParams(params) {
    params = params || {};

    let host = this._lastAbortRequest && params.failover !== false ? this._failover.client.transport.connectionPool.getAllHosts()[0] : this.host,
        reqParams = {
            method: params.method || 'GET',
            protocol: host.protocol + ':',
            hostname: host.host,
            port: host.port,
            path: (host.path || '') + (params.path || ''),
            headers: host.getHeaders(params.headers),
            agent: this.agent
        };

    if (!reqParams.path) {
        reqParams.path = '/';
    }

    let query = host.getQuery(params.query);
    if (query) {
        reqParams.path = reqParams.path + '?' + qs.stringify(query);
    }
    
    return reqParams;
}
