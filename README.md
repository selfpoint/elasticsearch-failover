# elasticsearch-failover
`HttpConnector` for `elasticsearch` thad add ability to configure failover client.
for more details about `HttpConnector` [click here](https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/extending_core_components.html).
## config
```javascript
config.failover {Object}
config.failover.client {Client}
config.failover.retryTime {Number=600000}
```

## example
```javascript
'use strict';

const elasticsearch = require('elasticsearch'),
    elasticsearchFailover = require('elasticsearch-failover'),
    client = new elasticsearch.Client({
        host: '...',
        connectionClass: elasticsearchFailover,
        failover: {
            client: new elasticsearch.Client({
                host: '...'
            }),
            retryTime: 600000 // 10 minutes
        }
    });
```
